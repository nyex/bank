class Money
{
private:
    int64_t amount;
    Currency::Currency currency;

public:
    Money(/* args */);
    ~Money();

    void SetAmount(int64_t amount);
    void SetCurrency(Currency::Currency currency);

    int64_t GetAmount();
    Currency::Currency getCurrency();
};
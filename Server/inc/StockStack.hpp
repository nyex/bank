class StockStack
{
private:
    Stock stock;
    uint16_t stock_count;

public:
    StockStack(/* args */);
    ~StockStack();

    void SetStock(Stock stock);
    void SetStockCount(uint16_t stock_count);

    Stock GetStock();
    uint16_t GetStockCount();


};

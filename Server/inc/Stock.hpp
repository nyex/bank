class Stock
{
private:
    uint16_t value;
    StockName::StockName stock_name;

public:
    Stock(/* args */);
    ~Stock();

    void SetValue(uint16_t value);
    void SetName(StockName::StockName stock_name);

    uint16_t GetValue();
    StockName::StockName GetName();
};

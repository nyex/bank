class MoneyWallet
{
private:
    std::map<Currency::Currency, Money> moneys;

public:
    MoneyWallet();
    ~MoneyWallet();

    bool DepositMoney(Currency::Currency currency, int64_t value);
    bool WithdrawMoney(Currency::Currency currency, int64_t value);
};

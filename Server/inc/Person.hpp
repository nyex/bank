class Person
{
private:
    std::string name;
    std::string document;

public:
    Person();
    ~Person();

    void SetName(std::string name);
    void SetDocument(std::string document);

    std::string GetName();
    std::string GetDocument();
};
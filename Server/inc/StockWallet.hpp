class StockWallet
{
private:
    std::map<StockName::StockName, StockStack> stock_stack;

    Stock CreateStock(StockName::StockName stock_name, uint64_t stock_value);
    StockStack CreateStockStack(Stock stock, uint16_t stock_count);

public:
    StockWallet(/* args */);
    ~StockWallet();

    bool BuyStock(StockName::StockName stock_name, uint16_t stock_count, uint64_t stock_value);
};


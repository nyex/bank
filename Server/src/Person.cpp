#include <string>
#include <iostream>

#include "Person.hpp"

Person::Person(/* args */)
{
}

Person::~Person()
{
}

void Person::SetName(std::string name) {
    this->name = name;
}

void Person::SetDocument(std::string document) {
    this->document = document;
}

std::string Person::GetName() {
    return this->name;
}

std::string Person::GetDocument() {
    return this->document;
}
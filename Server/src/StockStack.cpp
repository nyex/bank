#include <stdint.h>

#include "Types.hpp"
#include "Stock.hpp"
#include "StockStack.hpp"

StockStack::StockStack(/* args */)
{
}

StockStack::~StockStack()
{
}

void StockStack::SetStock(Stock stock)
{
    this->stock = stock;
}

void StockStack::SetStockCount(uint16_t stock_count)
{
    this->stock_count = stock_count;
}

Stock StockStack::GetStock()
{
    return this->stock;
}

uint16_t StockStack::GetStockCount()
{
    return this->stock_count;
}

#include <map>
#include <cstdint>
#include <iostream>

#include "Macros.hpp"
#include "Types.hpp"
#include "Stock.hpp"
#include "StockStack.hpp"
#include "StockWallet.hpp"

StockWallet::StockWallet(/* args */)
{
}

StockWallet::~StockWallet()
{
}

Stock StockWallet::CreateStock(StockName::StockName stock_name, uint64_t stock_value) {
    Stock stock;

    //TEST_TO_RETURN(stock_name > StockName::MAX, NULL);

    stock.SetName(stock_name);
    stock.SetValue(stock_value);

    return stock;
}

StockStack StockWallet::CreateStockStack(Stock stock, uint16_t stock_count) {
    StockStack stock_stack;

    stock_stack.SetStock(stock);
    stock_stack.SetStockCount(stock_count);

    return stock_stack;
}


bool StockWallet::BuyStock(StockName::StockName stock_name, uint16_t stock_count, uint64_t stock_value) {
    Stock stock;

    stock = StockWallet::CreateStock(stock_name, stock_value);
    stock_count = stock_count;
    //TEST_TO_RETURN(stock == NULL, false);

    return true;
}
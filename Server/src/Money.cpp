#include <stdint.h>
#include "Types.hpp"
#include "Money.hpp"

Money::Money(/* args */)
{
}

Money::~Money()
{
}

void Money::SetAmount(int64_t amount)
{
    this->amount = amount;
}
void Money::SetCurrency(Currency::Currency currency)
{
    this->currency = currency;
}

int64_t Money::GetAmount()
{
    return this->amount;
}

Currency::Currency Money::getCurrency()
{
    return this->currency;
}
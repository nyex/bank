#include <stdint.h>

#include "Types.hpp"
#include "Stock.hpp"

Stock::Stock(/* args */)
{
}

Stock::~Stock()
{
}

void Stock::SetValue(uint16_t value)
{
    this->value = value;
}

void Stock::SetName(StockName::StockName stock_name)
{
    this->stock_name = stock_name;
}

uint16_t Stock::GetValue()
{
    return this->value;
}

StockName::StockName Stock::GetName()
{
    return this->stock_name;
}
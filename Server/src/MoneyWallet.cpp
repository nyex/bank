#include <map>
#include <stdint.h>

#include "Types.hpp"
#include "Macros.hpp"
#include "Money.hpp"
#include "MoneyWallet.hpp"

MoneyWallet::MoneyWallet()
{
}

MoneyWallet::~MoneyWallet()
{
}

bool MoneyWallet::DepositMoney(Currency::Currency currency, int64_t value)
{
    Money money;

    TEST_TO_RETURN(value <= 0, false);
    TEST_TO_RETURN(Currency::MAX <= currency, false);

    if (this->moneys.count(currency) > 0)
    {
        value = value + this->moneys[currency].GetAmount();
    }

    money.SetAmount(value);
    money.SetCurrency(currency);
    this->moneys[currency] = money;

    return true;
}

bool MoneyWallet::WithdrawMoney(Currency::Currency currency, int64_t value)
{
    Money money;

    TEST_TO_RETURN(value <= 0, false);
    TEST_TO_RETURN(Currency::MAX <= currency, false);
    TEST_TO_RETURN(this->moneys.count(currency) <= 0, false);
    TEST_TO_RETURN(this->moneys[currency].GetAmount() < value, false);

    money = this->moneys[currency];
    value = money.GetAmount() - value;
    money.SetAmount(value);
    this->moneys[currency] = money;

    return true;
}
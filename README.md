# Bank

Servidor responsavel do sistema de banco

```mermaid
classDiagram
    Types <|-- Money
    Types <|-- MoneyWallet
    Types <|-- Stock
    Types <|-- StockWallet
    Money <|-- MoneyWallet
    MoneyWallet <|-- Patrimony
    StockWallet <|-- Patrimony
    Stock <|-- StockStack
    StockStack <|-- StockWallet
    Patrimony <|-- Person

    class Types{
        enum Currency
        enum StockName
    }

    class Money {
        - int64_t amount
        - Currency currency
    }

    class MoneyWallet {
        - map < Currency, Money > moneys
    }

    class Patrimony {
        - MoneyWallet money_wallet
    }

    class Person {
        - string name
        - string document
        - Patrimony patrimony
    }

    class Stock {
        - uint16_t value
        - StockName stock_name
    }

    class StockStack {
        - Stock stock
        - uint16_t stock_count
    }

    class StockWallet {
        - map < StockName , StockStack > stock_stack
    }

```
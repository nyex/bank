#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /// Signals & Slot
    connect(ui->btn_register, SIGNAL(clicked()), this, SLOT(account_register()));
    connect(ui->btn_login, SIGNAL(clicked()), this, SLOT(account_login()));
    connect(ui->btn_exit, SIGNAL(clicked()), this, SLOT(exit_system()));

    /// Init design
    MainWindow::initialize_design();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::account_register() {
    qDebug() << __FUNCTION__ << " Selecionada [ File: " << __FILE_NAME__ << " / Line: " << __LINE__ << " ]";
    ui->statusbar->showMessage("Waiting for server...");
}

void MainWindow::account_login() {
    qDebug() << __FUNCTION__ << " Selecionada [ File: " << __FILE_NAME__ << " / Line: " << __LINE__ << " ]";
    ui->statusbar->showMessage("Connecting...");
}

void MainWindow::exit_system() {
    qDebug() << __FUNCTION__ << " Selecionada [ File: " << __FILE_NAME__ << " / Line: " << __LINE__ << " ]";
    this->close();
}

void MainWindow::initialize_design() {
    ui->img_logo->setPixmap(QPixmap(":/Imgs/imgs/logo.png"));
    ui->img_logo->setMaximumHeight(250);
    ui->img_logo->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::MinimumExpanding);
    ui->img_logo->setScaledContents(true);

    ui->txt_acnt->setAlignment(Qt::AlignCenter);
    ui->txt_acnt->setPlaceholderText("Account number");
    ui->txt_acnt->setStyleSheet("color: #FFFFFF; padding: 8px;");

    ui->txt_pswd->setAlignment(Qt::AlignCenter);
    ui->txt_pswd->setPlaceholderText("Password");
    ui->txt_pswd->setStyleSheet("color: #FFFFFF; padding: 8px;");
    ui->txt_pswd->setEchoMode(QLineEdit::EchoMode::Password);

    ui->btn_login->setText("Connect");
    ui->btn_login->setStyleSheet("background-color: #71D99E;\ncolor: rgb(255, 255, 255);\nborder: 1px solid #55736D;\npadding: 7px;\nfont: bold;");

    ui->btn_register->setFlat(true);
    ui->btn_register->setText("Register");
    ui->btn_register->setStyleSheet("color: #55736D;\nfont: bold;");

    ui->btn_exit->setFlat(true);
    ui->btn_exit->setText("Exit");
    ui->btn_exit->setStyleSheet("color: #55736D;\nfont: bold;");

    ui->lbl_version->setText(VERSION_CLIENT);
    ui->lbl_version->setAlignment(Qt::AlignmentFlag::AlignCenter);
}

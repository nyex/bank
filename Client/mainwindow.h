#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#define VERSION_CLIENT "Version: 0.1 - Alpha"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void account_register();
    void account_login();
    void exit_system();

private:
    Ui::MainWindow *ui;
    void initialize_design();
};
#endif // MAINWINDOW_H
